﻿#include <iostream>

void PrintEvenOrOddNumbers(int maxNumber, bool isOdd = false) {
    std::cout << (isOdd ? "Odd" : "Even") << " numbers from 0 to " << maxNumber << std::endl;

    for (int i = isOdd; i <= maxNumber; i += 2) {
        std::cout << i << std::endl;
    }
}

int main()
{
    const int N = 16;

    PrintEvenOrOddNumbers(N);
    PrintEvenOrOddNumbers(N, true);
}
